resource "vault_auth_backend" "approle" {
  type = "approle"
}

resource "vault_approle_auth_backend_role" "role_id" {
  for_each = { for i in var.approles: i.role_name => i if i.enable }
  backend         = vault_auth_backend.approle.path
  role_name       = each.value["role_name"]
  token_policies  = each.value["token_policies"]
  secret_id_ttl   = each.value["secret_id_ttl"]
  secret_id_num_uses  = each.value["secret_id_num_uses"]
  token_num_uses  = each.value["token_num_uses"]
#  token_no_default_policy = true
#  token_ttl = 86400
}

resource "vault_approle_auth_backend_role_secret_id" "secret_id" {
  for_each = { for i in var.approles: i.role_name => i if i.enable }
  backend   = vault_auth_backend.approle.path
  role_name       = vault_approle_auth_backend_role.role_id[each.key].role_name
}

output "role_id" {
  value = { for i in vault_approle_auth_backend_role.role_id: i.role_name => i.role_id }
  sensitive = false
}

output "secret_id" {
  value = { for i in vault_approle_auth_backend_role_secret_id.secret_id: i.role_name => i.secret_id }
  sensitive = true
}
