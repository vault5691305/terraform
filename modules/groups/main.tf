resource "vault_identity_group" "groups" {
  name     = var.groups[count.index]["name"]
  type     = var.groups[count.index]["type"]
  policies = var.groups[count.index]["policy"]

  count      = length(var.groups)
}

resource "vault_identity_group_alias" "groups-alias" {
  name           = var.groups[count.index]["alias"]
  mount_accessor = var.oidc_mount_accessor
  canonical_id   = vault_identity_group.groups[count.index]["id"]

  count      = length(var.groups)
}
