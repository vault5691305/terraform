resource "vault_policy" "policies" {
  for_each = toset([ for k in fileset("${var.policies_path}","*.hcl"): trimsuffix(k,".hcl")])
  name = each.value
  policy = file("${var.policies_path}/${each.value}.hcl")
}
