variable "login_oidc_client_id" {
  type = string
}
variable "login_oidc_client_secret" {
  type = string
  sensitive = true
}
variable "groups" {}
variable "k8s_roles" {}
variable "gitlab_roles" {}
variable "s3_bucket_backup_conf" {}
variable "s3_region" {}
variable "s3_access_key" {
  sensitive = true
}
variable "s3_secret_key" { 
  sensitive = true
}
variable "s3_endpoint" {}
variable "s3_lock_enable" {}
variable "s3_lifecycle_enable" {}

variable "gitlab_url" {}
variable "oidc_url" {}
variable "ingress_dns" {}
variable "k8s_context_config" {}
variable "cluster_issuer" {}
variable "vault_version" {}
variable "ingress_whitelist" {
  type = list(string)
}
variable "snap_schedule" {}
variable "vault_keycloak_conf_enable" {}
variable "vault_gitlab_conf_enable" {}
variable "vault_s3_backup_enable" {}
variable "vault_replica" {}
variable "approles" {}
