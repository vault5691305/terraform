path "sys/storage/raft/snapshot" {
   capabilities = ["read"]
}
path "secret/data/snapshot/s3" {
   capabilities = ["read"]
}
