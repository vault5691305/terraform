[TOC]
# Deploy Vault Helm Chart and Maintain its Configuration with Terraform
## Deployment Architecture
![image info](images/Vault_Archi.gif "Vault Deployment Architecture"){width=50% height=50%}
## Kubernetes requirements
- Cert-manager with letsencrypt cluster issuer configured
- Nginx Ingress controller
- Kube-prometheus-stack

## Apply !
Suit the "terraform.tfvars" to your needs, then launch Terraform commands:  
```bash
terraform init
```
```bash
terraform plan
```
```bash
terraform apply
```
At the end, you'll get a working ready-to-use vault instance initialized,unsealed, configured.

## Get Root Token and Unseal keys
After you had successful applied terraform, you can get the Vault root token and unseal keys in two ways:  

- Through Terraform  
```bash
# Get root token
terraform output root-token
```
```bash
# Get unseal keys
terraform output unseal-keys
```
- Through Kubernetes  
```bash
# Get root token
kubectl get secrets -n vault vaultsecret -o jsonpath='{.data.root-token}' |base64 -d;echo
```
```bash
# Get unseal keys
kubectl get secrets -n vault vaultsecret -o jsonpath='{.data.unseal-key}' |base64 -d;echo
```
## Vault Backup/Snapshot
Vault Backup are saved through a cronjob, under the namespace **vault-backup** with the name **vault-snapshot**, backups are uploaded to the bucket name format:   
&lt;vault-ingress-dns&gt;-daily or &lt;vault-ingress-dns&gt;-monthly  
### Restore Snapshot
You can use the pod-restore.yaml manifest in this repo.  
