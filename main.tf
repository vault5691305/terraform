provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = var.k8s_context_config
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
    config_context = var.k8s_context_config
  }
}

provider "vault" {
  address = "https://${var.ingress_dns}"
  token = data.kubernetes_secret.roottoken.data.root-token
  skip_tls_verify = true
}

output "root-token" {
  value = data.kubernetes_secret.roottoken.data.root-token
  sensitive = true
}

output "unseal-keys" {
  value = data.kubernetes_secret.roottoken.data.unseal-key
  sensitive = true
}

resource "kubernetes_namespace" "vault_ns" {
  metadata {
    name = "vault"
  }
}

resource "kubernetes_manifest" "vault_sa" {
  for_each = toset(split("---",file("vaultsecret.yaml")))
  manifest = yamldecode(each.value)

  depends_on = [kubernetes_namespace.vault_ns]
}

resource "kubernetes_config_map" "vaultinit" {
  metadata {
    name = "vaultinit"
    namespace = "vault"
  }
  data = {
    "vaultinit.sh" = file("vaultinit.sh")
  }
  depends_on = [kubernetes_manifest.vault_sa]
}

resource "helm_release" "vault_helm" {
  name       = "vault"
  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault"
  namespace  = "vault"
  recreate_pods = true
  wait       = true
  timeout    = 600
  max_history = 3
  create_namespace = true
#  version    = ""
  values = [ "${templatefile("values.tftpl",
                {vault_url = var.ingress_dns,
                 issuer = var.cluster_issuer,
                 vault_version = var.vault_version,
                 vault_replica = var.vault_replica,
                 ingress_whitelist = var.ingress_whitelist})}"
           ]

  depends_on = [kubernetes_config_map.vaultinit]
}

resource "helm_release" "vault_banzai" {
  name       = "vault-banzai-secrets"
  repository = "oci://ghcr.io/bank-vaults/helm-charts"
  chart      = "vault-secrets-webhook"
  namespace  = "vault"
  recreate_pods = true
  wait       = true
  timeout    = 600
  max_history = 3

  depends_on = [kubernetes_job.vault_status]
}

module "base_policies" {
  source = "./modules/policy"
  policies_path = "base_policies"
}

data "kubernetes_secret" "roottoken" {
  metadata {
    namespace = "vault"
    name = "vaultsecret"
  }
  depends_on = [kubernetes_job.vault_status]
}

resource "kubernetes_job" "vault_status" {
  metadata {
    name = "vault-status"
    namespace = "vault"
  }
  spec {
    template {
      metadata {}
      spec {
        container {
          image = "hashicorp/vault:${var.vault_version}"
          name = "vault-status"
          command = ["sh","-c",<<EOT
            RETVAL=2;while [ $RETVAL -ne 0 ];\
            do OUTPUT=`vault status`; RETVAL=$?; sleep 10;done
            EOT
            ]
          env {
            name = "VAULT_ADDR"
            value = "http://vault-active:8200"
          }
        }
        restart_policy = "Never"
      }
    }
  }
  wait_for_completion = true
  timeouts {
    create = "10m"
    update = "10m"
  }
}
