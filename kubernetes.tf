resource "vault_auth_backend" "kubernetes" {
  type = "kubernetes"
}

resource "vault_kubernetes_auth_backend_config" "kubernetes" {
  backend                =  vault_auth_backend.kubernetes.path
  kubernetes_host        = "https://kubernetes.default.svc.cluster.local"
}

resource "vault_kubernetes_auth_backend_role" "k8s-roles" {
  backend = vault_auth_backend.kubernetes.path
  role_name = var.k8s_roles[count.index]["role_name"]
  bound_service_account_names = var.k8s_roles[count.index]["bound_service_account_names"]
  bound_service_account_namespaces  = var.k8s_roles[count.index]["bound_service_account_namespaces"]
  token_ttl = var.k8s_roles[count.index]["token_ttl"]
  token_policies = var.k8s_roles[count.index]["token_policies"]
  token_no_default_policy = var.k8s_roles[count.index]["token_no_default_policy"]

  count    = length(var.k8s_roles)
}
