provider "aws" {
  endpoints {
    s3 = var.s3_endpoint
  }
  region     = var.s3_region
  access_key = var.vault_s3_backup_enable ? var.s3_access_key : null
  secret_key = var.vault_s3_backup_enable ? var.s3_secret_key : null

}

resource "aws_s3_bucket" "s3_bucket_backup" {
  count = var.vault_s3_backup_enable ? length(var.s3_bucket_backup_conf) : 0
  bucket = "${var.ingress_dns}-${var.s3_bucket_backup_conf[count.index].name}"
  object_lock_enabled = var.s3_lock_enable ? "true" : "false"
}

resource "aws_s3_bucket_object_lock_configuration" "s3_bucket_backup_lock" {
  count = var.vault_s3_backup_enable && var.s3_lock_enable ? length(var.s3_bucket_backup_conf) : 0
  bucket = aws_s3_bucket.s3_bucket_backup[count.index].bucket
  rule {
    default_retention {
      mode = "COMPLIANCE"
      days = var.s3_bucket_backup_conf[count.index].lock_days
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "s3_bucket_backup_lifecycle" {
  count = var.vault_s3_backup_enable && var.s3_lifecycle_enable ? length(var.s3_bucket_backup_conf) : 0
  bucket = aws_s3_bucket.s3_bucket_backup[count.index].bucket
  rule {
    id = "lifecycle"
    status = "Enabled"
    expiration {
      days = var.s3_bucket_backup_conf[count.index].exp_days
    }
  }
}

resource "kubernetes_namespace" "vault_snapshot_ns" {
  metadata {
    name = "vault-backup"
  }
  count = var.vault_s3_backup_enable ? 1 : 0
}

resource "kubernetes_service_account" "snapshot_sa" {
  metadata {
    name = "snapshoter"
    namespace = "vault-backup"
  }
  count = var.vault_s3_backup_enable ? 1 : 0
}

resource "kubernetes_manifest" "snapshot" {
  manifest = yamldecode(templatefile("snapshot.tftpl",
                {schedule = var.snap_schedule,
                 vault_version = var.vault_version,
                 ingress_dns = var.ingress_dns}))

  count = var.vault_s3_backup_enable ? 1 : 0
}

module "snapshot_policies" {
  source = "./modules/policy"
  policies_path = "snapshot_policies"
## disabling temporary
#  count = var.vault_s3_backup_enable ? 1 : 0
}
################ Snapshot approle is not needed since kubernetes auth/role is already enabled, it's just for testing the module
module "approle_snapshot" {
  source = "./modules/approles"
  approles = var.approles

#  count = var.vault_s3_backup_enable ? 1 : 0
}

output "snapshot_role_id" {
  value = try(module.approle_snapshot.role_id.snapshot_role,null)
}

output "snapshot_secret_id" {
  value = try(module.approle_snapshot.secret_id.snapshot_role,null)
  sensitive = true
}
##########################################################################
resource "vault_mount" "secret" {
  path        = "secret"
  type        = "kv"
  options     = { version = "2" }
  description = "KV Version 2 secret engine mount"

  count = var.vault_s3_backup_enable ? 1 : 0
}

resource "vault_kv_secret_v2" "s3_secret" {
  mount                      = vault_mount.secret[count.index].path
  name                       = "snapshot/s3"
  cas                        = 1
  data_json                  = jsonencode(
  {
    access_key = "${var.s3_access_key}",
    secret_key = "${var.s3_secret_key}",
    region     = "${var.s3_region}",
    url        = "${var.s3_endpoint}"
  }
  )

  count = var.vault_s3_backup_enable ? 1 : 0
}

resource "vault_kubernetes_auth_backend_role" "k8s-snapshoter-role" {
  backend = vault_auth_backend.kubernetes.path
  role_name = "snapshoter"
  bound_service_account_names = ["snapshoter"]
  bound_service_account_namespaces  = ["vault-backup"] 
  token_ttl = 300
  token_policies = ["snapshot-readonly"]
  token_no_default_policy = "false" 

  count = var.vault_s3_backup_enable ? 1 : 0
}
