#!/bin/sh
set -e

SECRETREF=vaultsecret
UNSEAL_KEY_NUM=3
VAULT_FIRST=vault-0
LEADER_ADDR=http://${VAULT_FIRST}.vault-internal:8200
export VAULT_ADDR=http://127.0.0.1:8200
# Time to wait until give up leader dns resolution
TIMER=120
# define k8s api access and store the unseal key and root token in secrets
APISERVER=https://kubernetes.default.svc
SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount
NAMESPACE=$(cat ${SERVICEACCOUNT}/namespace)
TOKEN=$(cat ${SERVICEACCOUNT}/token)

vault_status() {
    if [ "${1}" == "${LEADER_ADDR}" ]
      then
      # Check the leader dns resolution
      resolvcheck=none
      while [ $TIMER -ne 0 ]
        do resolvcheck=`wget -q --spider ${LEADER_ADDR} &>/dev/null;echo $?`
           sleep 1
        if [ $resolvcheck -eq 0 ]
          then
            break
        fi
        let $((TIMER--))
        [ ${TIMER} -eq 0 ] && echo "can't resolve ${LEADER_ADDR}, exiting" && exit 1
      done
    fi
    # Check vault initialization
    init="none"
    while [[ "$init" != "false" && "$init" != "true" ]]
      do init=`vault status -address=${1:-$VAULT_ADDR} |grep -i 'init'|awk -F' ' '{print $2}'` || init="none"
         sleep 2
    done
}

unseal() {
	# Get vault k8s secret
	[ -z "${1}" ] && { wget -q --no-check-certificate --header "Authorization: Bearer ${TOKEN}" \
         ${APISERVER}/api/v1/namespaces/vault/secrets/${SECRETREF} \
         -O /tmp/${SECRETREF}.json && \
	UNSEALKEYS=`sed -nr 's/("unseal-key"): *"(.*)"/\2/p' /tmp/${SECRETREF}.json | base64 -d`; }
	for k in ${1:-$UNSEALKEYS}
	  do
            vault operator unseal ${k} > /dev/null
	done
}

if [ "${VAULT_K8S_POD_NAME}" == "${VAULT_FIRST}" ]
  then
    vault_status
    if [ "$init" == "false" ]
      then
          ALLKEYS=$(vault operator init \
             -key-shares=${UNSEAL_KEY_NUM} \
             -key-threshold=${UNSEAL_KEY_NUM} \
             -format=table |grep -Ei 'unseal.*key.*: |root.*token' |sed -E 's/(.*\:[[:blank:]]*)(.*)/\2/'| \
	      tr '\n' ' ')
          i=1
          for k in ${ALLKEYS}
            do
              if [[ $k =~ ^hvs\. ]]
                then ROOTTOKEN=$k
              else
                export UNSEALKEY_${i}="$k"
                i=$((i+1))
              fi
          done
          UNSEALKEYS=`printenv | grep UNSEALKEY_|cut -d'=' -f2-|tr '\n' ' '`
          cat <<EOF >>/tmp/${SECRETREF}.json
          {
          "kind": "Secret",
          "apiVersion": "v1",
          "metadata": {
              "name": "${SECRETREF}",
              "namespace": "vault"
          },
          "stringData": {
              "root-token": "${ROOTTOKEN}",
              "unseal-key": "${UNSEALKEYS}"
          }
          }
EOF
          unseal "${UNSEALKEYS}"
          # Write to vault k8s secret ref
          wget -q --no-check-certificate --header "Authorization: Bearer ${TOKEN}" \
               --header "Content-Type: application/json" \
               --post-file=/tmp/${SECRETREF}.json ${APISERVER}/api/v1/namespaces/vault/secrets \
               -O- &>/dev/null
    elif [ "$init" == "true" ]
      then
        unseal
    fi
else
  # wait until the leader is ready
  vault_status ${LEADER_ADDR}
  unset init
  # check the status until the follower has been joined to the cluster
  while [ "$init" != "true" ]
    do vault_status
    vault operator raft \
      join -auto-join-scheme="http" -auto-join-port=8200 ${LEADER_ADDR}
    sleep 2
  done
  unseal
fi

# Delete the unseal keys payload
rm -rf /tmp/${SECRETREF}.json

exit 0
