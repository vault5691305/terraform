# Allow to list under kv path
path "kv/*"
{                                                      
  capabilities = ["read", "list"]
}
path "secret/*"
{                                                      
  capabilities = ["read", "list"]
}

path "kv/data/users"
{                                                                      
  capabilities = ["create", "read", "update", "delete", "list", "patch"]
}
path "kv/data/users/*"
{                                                                      
  capabilities = ["create", "read", "update", "delete", "list","patch"]
}

## test username path template
#path "secret/data/{{identity.entity.aliases.auth_oidc_49997b87.name}}/*" {
#  capabilities = ["create", "update", "patch", "read", "delete","list"]
#}
#path "secret/data/{{identity.entity.aliases.auth_oidc_49997b87.name}}" {
#  capabilities = ["create", "update", "patch", "read", "delete","list"]
#}
