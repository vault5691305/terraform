
resource "vault_jwt_auth_backend" "keycloak" {
    description         = "Keycloak config"
    path                = "oidc"
    type                = "oidc"
    oidc_discovery_url  = var.oidc_url
    oidc_client_id      = var.login_oidc_client_id
    oidc_client_secret  = var.login_oidc_client_secret
    default_role = "vault"

    count = var.vault_keycloak_conf_enable ? 1 : 0
}

resource "vault_jwt_auth_backend_role" "keycloak" {
  backend         = vault_jwt_auth_backend.keycloak[count.index].path
  role_name       = "vault"
  token_policies  = ["generic-users"]
  token_no_default_policy = "false"
  user_claim            = "email"
  groups_claim		= "groups"
  role_type             = "oidc"
  allowed_redirect_uris = ["https://${var.ingress_dns}/oidc/oidc/callback", "https://${var.ingress_dns}/ui/vault/auth/oidc/oidc/callback"]

  count = var.vault_keycloak_conf_enable ? 1 : 0
}

module "groups" {
  source = "./modules/groups"
  groups = var.groups
  oidc_mount_accessor = vault_jwt_auth_backend.keycloak[count.index].accessor

  count = var.vault_keycloak_conf_enable ? 1 : 0
}
