vault_version = "1.15.2"
# K8s config to use kubernetes provider
k8s_context_config = "mycontext"
vault_replica = 3

############## Ingress Config
cluster_issuer = "letsencrypt-staging"
ingress_dns = "myvault.server.com"
ingress_whitelist = ["xxx.xxx.xxx.xxx/32","xxx.xxx.xxx.xxx/32"]
############################

############# Enable S3 backup
vault_s3_backup_enable = false
s3_access_key = "xxxxxxxxxxxxxxxxxxxx"
s3_secret_key = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
s3_region = "eu-west"
s3_endpoint = "https://s3.eu-west.mycloud.com"
snap_schedule = "* 7 * * *"
s3_lock_enable = false
s3_lifecycle_enable = true
s3_bucket_backup_conf = [
  { 
    name = "daily"
    exp_days = 8
    lock_days = 7
  },
  {
    name = "monthly"
    exp_days = 365
    lock_days = 30
  }
]

###############

############# Enable keycloak config
vault_keycloak_conf_enable = false
login_oidc_client_id = "my-vault"
login_oidc_client_secret = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
oidc_url = "https://myoidc.server.com/auth/realms/MYREALM" 
groups = [
  {
    name = "vault-admins"
    alias = "my-vault-admins"
    type = "external"
    policy = ["vault-admins"]
  }
]
################

########### Enable Gitlab auth config for CI Pipeline Auth
vault_gitlab_conf_enable = false
gitlab_url = "https://gitlab.com" 
gitlab_roles = [
  {
    role_name = "gitlab-readonly"
    user_claim		    = "user_email"
    token_policies          = ["gitlab-readonly"]
    token_no_default_policy = "false"
    bound_audiences         = ["https://gitlab.com"]
    bound_claims_type       = "string"
    bound_claims	    = { 
      "project_id" = "1111111",
      "ref_type": "branch",
      "ref": "main"
    }
  }
]
#################

# Various vault role allowed read secret for k8S pods
k8s_roles = [
  {
    role_name = "wordpress"
    bound_service_account_names      = ["wordpress"]
    bound_service_account_namespaces = ["wordpress"]
    token_ttl                        = 120
    token_policies                   = ["wordpress-readonly"]
    token_no_default_policy          = "false"
  }
]

approles = [
  { 
    enable = true
    role_name = "snapshot_role"
    token_policies = ["snapshot-readonly"]
    token_num_uses = 0
    secret_id_num_uses = 0
    secret_id_ttl = 0
  }
]
