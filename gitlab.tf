
resource "vault_jwt_auth_backend" "gitlab" {
    description         = "JWT gitlab config for Gitlab CI"
    path                = "gitlab"
    type                = "jwt"
    oidc_discovery_url  = var.gitlab_url
    bound_issuer        = var.gitlab_url

    count = var.vault_gitlab_conf_enable ? 1 : 0
}

resource "vault_jwt_auth_backend_role" "gitlab" {
  backend         = vault_jwt_auth_backend.gitlab[count.index].path
  role_type	  = "jwt"
  token_no_default_policy = var.gitlab_roles[count.index]["token_no_default_policy"]
  role_name       = var.gitlab_roles[count.index]["role_name"]
  token_policies  = var.gitlab_roles[count.index]["token_policies"]
  bound_audiences = var.gitlab_roles[count.index]["bound_audiences"]
  user_claim            = var.gitlab_roles[count.index]["user_claim"]
  bound_claims_type	= var.gitlab_roles[count.index]["bound_claims_type"]
  bound_claims		= var.gitlab_roles[count.index]["bound_claims"]

  count	          = var.vault_gitlab_conf_enable ? length(var.gitlab_roles) : 0
}

module "gitlab_policies" {
  source = "./modules/policy"
  policies_path = "gitlab_policies"

  count = var.vault_gitlab_conf_enable ? 1 : 0
}
